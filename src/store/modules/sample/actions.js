const actions = {
    fetchSampleData(context) {
        fetch('https://jsonplaceholder.typicode.com/posts/1')
            .then(response => response.json())
            .then(json => {
                context.commit('setData', json)
            })
    }
}

export default actions