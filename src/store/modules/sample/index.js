import state from './state'
import mutations from './mutations'
import actions from './actions'

const sampleStoreModule = {
    state,
    mutations,
    actions
}

export default sampleStoreModule