import sampleStoreModule from './modules/sample'

const storeConfig = {
    state: {
        loading: true
    },
    modules: {
        sample: sampleStoreModule
    }
}

export default storeConfig