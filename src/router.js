import Home from './pages/home/index.vue'
import About from './pages/about/index.vue'
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/about',
        component: About
    }
]

const createRouter = () => {
    return new Router({
        mode: 'history',
        routes
    })
}

export default createRouter