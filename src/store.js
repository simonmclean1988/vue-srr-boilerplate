import Vue from 'vue'
import Vuex from 'vuex'
import storeConfig from './store/'

Vue.use(Vuex)

const createStore = () => new Vuex.Store(storeConfig)

export default createStore