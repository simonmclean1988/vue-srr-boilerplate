# Vue SSR Boilerplate

Minimal Vue SSR boilerplate, with Vue Router, plus Vuex for state management.

Webpack configs and `server.js` based on https://alligator.io/vuejs/basic-ssr/

## Commands

### Setup
```
npm install
```

### Development
```
$ npm run dev
```

### Production
```
$ npm run build
$ npm run serve
```
